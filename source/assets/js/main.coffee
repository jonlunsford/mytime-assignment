# Main App

# Config

App = Ember.Application.create
  LOG_TRANSITIONS: true

App.ENDPOINT = "/assets/deals.json"

# Routes

App.IndexRoute = Ember.Route.extend
  model: ->
    Ember.$.getJSON App.ENDPOINT, (data) -> data

# Helpers

Ember.Handlebars.helper "format-currency", (value, options) ->
  "$#{value.toFixed(2)}"

Ember.Handlebars.helper "format-date", (value) ->
  date = new Date(value[0])
  "#{date.getMonth() + 1}/#{date.getDate()}/#{date.getFullYear()}"
